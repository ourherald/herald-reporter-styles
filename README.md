# Herald Reporter Styles

This repository contains the template files required for properly formatting Herald articles. It is intended to be used by config management software such as Ansible to deploy the correct templates to the appropriate users.